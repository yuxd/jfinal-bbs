package cn.iver.ext.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;
import com.jfinal.render.RenderFactory;

/**
 * 用于beetl-UI测试
 */
public class ViewTestHandler extends Handler {

    @Override
    public void handle(String target, HttpServletRequest request,
            HttpServletResponse response, boolean[] isHandled) {
        if (target.startsWith("/html")) {
            String view = target.substring(0, target.lastIndexOf('.'));
            RenderFactory.me().getDefaultRender("_dev" + view).setContext(request, response).render();
            // 跳出
            isHandled[0] = true;
            return;
        }
        nextHandler.handle(target, request, response, isHandled);
    }

}