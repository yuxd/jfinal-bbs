package cn.iver.validator;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;
/**
 * Created with IntelliJ IDEA.
 * Author: StevenChow
 * Date: 13-5-8
 */
public class LoginValidator extends Validator{
    @Override
    protected void validate(Controller c) {
       /* validateEmail("email", "msg", "错误的邮箱地址");*/
        validateRequired("email","msg","用户名不能为空");
        validateRequired("password", "msg", "密码不能为空");

        validateEqualString(c.getPara("token"),"pdd*V*m5CVMkVo%C","msg","非法登录，不是从OA发起的集成登录");
    }

    @Override
    protected void handleError(Controller c) {
        c.keepPara("email");
        c.render("/user/login.html");
    }
}
