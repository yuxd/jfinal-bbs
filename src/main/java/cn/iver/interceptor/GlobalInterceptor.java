package cn.iver.interceptor;

import cn.iver.common.Const;
import cn.iver.model.Module;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

/**
 * Created with IntelliJ IDEA.
 * Author: StevenChow
 * Date: 13-3-30
 */
public class GlobalInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation inv) {
        Controller controller = inv.getController();

        controller.setAttr("moduleList", Module.dao.getList());

        inv.invoke();

        controller.setAttr("v", Const.TIMESTAMP);
    }
}
