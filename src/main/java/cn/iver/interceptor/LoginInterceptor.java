package cn.iver.interceptor;

import cn.iver.common.WebUtils;
import cn.iver.model.User;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

/**
 * Created with IntelliJ IDEA.
 * Author: StevenChow
 * Date: 13-5-7
 */
public class LoginInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation inv) {
        Controller controller = inv.getController();
        User user = WebUtils.currentUser(controller);

        if(user != null){
            inv.invoke();
        }else{
            controller.setAttr("msg", "需要登录才可以进行改操作：）");
            controller.render("/user/login.html");
        }
    }
}
